import os
from collections import namedtuple

ROOT_DIR = '\\'.join(os.path.dirname(os.path.abspath(__file__)).split('\\')[:-1])
RESOURCES_DIR = ROOT_DIR + '\\resources\\'

WINDOW_RESOLUTION = {
    'main': {
        'x': 300,
        'y': 400},
}

ALLOWED_VIDEO_TYPES = "MP4 (*.mp4 *.m4a *.m4v *.f4v *.f4a *.m4b *.m4r *.f4b *.mov);;" \
                      "3GP (*.3gp *.3gp2 *.3g2 *.3gpp *.3gpp2);;" \
                      "OGG (*.ogg *.oga *.ogv *.ogx);;" \
                      "WMV (*.wmv *.wma *.asf*);;" \
                      "WEBM (*.webm);;" \
                      "FLV (*.flv);;" \
                      "AVI (*.avi*);;" \
                      "MPEG-TS (*.ts)"

# APP_SHORTCUTS_BASE = namedtuple('AppShortcuts',
#                                 ['escape',
#                                  'forward_play',
#                                  'backward_play',
#                                  'increase_step',
#                                  'decrease_step',
#                                  'marker_mode',
#                                  'open_video',
#                                  'open_data',
#                                  'open_path',
#                                  'create_project',
#                                  'open_project',
#                                  'import_tags',
#                                  'export_tags',
#                                  'full_screen',
#                                  'increase_app',
#                                  'decrease_app'])
#
# APP_SHORTCUTS = APP_SHORTCUTS_BASE(escape=Qt.Key_Escape,
#                                    forward_play=Qt.Key_Period,
#                                    backward_play=Qt.Key_Comma,
#                                    increase_step=Qt.Key_BraceRight,
#                                    decrease_step=Qt.Key_BraceLeft,
#                                    marker_mode=Qt.Key_Alt,
#                                    open_video='Ctrl+V',
#                                    open_data='Ctrl+D',
#                                    open_path='Ctrl+P',
#                                    create_project='Ctrl+N',
#                                    open_project='Ctrl+O',
#                                    import_tags='Ctrl+J',
#                                    export_tags='Ctrl+E',
#                                    full_screen='F10',
#                                    increase_app='+',
#                                    decrease_app='-')
