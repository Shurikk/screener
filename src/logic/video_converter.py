import os
import moviepy.editor as mp
import ffmpy


def time_symetrize(clip):
    return mp.concatenate([clip, clip.fx(mp.vfx.time_mirror)])


def convert_to_gif(video_file, gif_file=None, fps=10):
    output_file = gif_file or os.path.splitext(video_file)[0] + '.gif'
    clip = (mp.VideoFileClip(video_file)
            .subclip((0, 6), (0, 45))
            .resize(0.5)
            # .fx(time_symetrize)
            )
    clip.write_gif(output_file, fps=fps, fuzz=2)


def convert_to_video(gif_file, video_file=None):
    output_file = video_file or os.path.splitext(gif_file)[0] + '.mp4'
    clip = mp.VideoFileClip(gif_file)
    clip.write_videofile(output_file)


def convert_to_video_2(gif_file, video_file=None):
    output_file = video_file or os.path.splitext(gif_file)[0] + '.mp4'
    ff = ffmpy.FFmpeg(
        inputs={gif_file: None},
        outputs={output_file: None}
    )
    ff.run()
