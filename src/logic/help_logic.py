from functools import wraps


def static_vars(**kwargs):
    @wraps(static_vars)
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func

    return decorate


class BaseTypeData:
    def __init__(self, value):
        self.subscriptions = []
        self.value = value

    def __str__(self):
        return str(self.value)

    @property
    def value(self):
        return self.__value

    @value.setter
    def value(self, new_value):
        self.__value = new_value
        self.on_value_changed(new_value)

    def on_value_changed(self, new_value):
        for fun in self.subscriptions:
            fun(new_value)

    def subscribe(self, fun):
        self.subscriptions.append(fun)

    def unsubscribe(self, fun):
        self.subscriptions.remove(fun)


class BoolData(BaseTypeData):
    def __init__(self, value):
        super(BoolData, self).__init__(value=value)

    def __bool__(self):
        return self.value or self.value == 1


class IntData(BaseTypeData):
    def __init__(self, value):
        super(IntData, self).__init__(value=value)

    def __int__(self):
        return self.value
