import numpy as np
import cv2
import time
import mss

from src.logic.help_logic import BoolData


def capture(frame_width=1280, frame_height=720, top_offset=0, left_offset=0, fps=20.0,
            show_fps=False, save_path='screen_capture.avi',
            locker=BoolData(True), pause=BoolData(False), cap_out=None):
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    out = cap_out or cv2.VideoWriter(save_path, fourcc, fps, (frame_width, frame_height))

    with mss.mss() as sct:
        # Part of the screen to capture
        monitor = {"top": top_offset, "left": left_offset, "width": frame_width, "height": frame_height}

        while "Screen capturing" and bool(locker):
            if bool(pause):
                return out
            last_time = time.time()

            # Get raw pixels from the screen, save it to a Numpy array
            img = np.array(sct.grab(monitor))
            img = cv2.resize(img, (frame_width, frame_height))
            frame = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

            if show_fps:
                cv2.putText(frame, "FPS: %f" % (1.0 / (time.time() - last_time)),
                            (10, 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
            out.write(frame)
            # cv2.imshow('frame', frame)

            # Press "q" to quit
            if cv2.waitKey(25) & 0xFF == ord("q"):
                break

    # Clean up
    out.release()
    cv2.destroyAllWindows()
