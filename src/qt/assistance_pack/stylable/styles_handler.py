import json
from PyQt5.QtGui import QPalette, QColor
from src.definitions import RESOURCES_DIR

WHITE = "#FFFFFF"
BLACK = "#000000"
RED = "#FF0000"
PRIMARY = "#353535"
SECONDARY = "#232323"
TERTIARY = "#2A82DA"
DARK_GRAY = "#808080"

with open(RESOURCES_DIR + 'styles.json', 'r') as f:
    STYLES = json.load(f)


def get_styles():
    return list(STYLES.keys())


def css_rgb(color, a=False):
    return ("rgba({}, {}, {}, {})" if a else "rgb({}, {}, {})").format(*color.getRgb())


def apply_style(app, style):
    custom_palette = QCustomPalette(**STYLES[style])
    custom_palette.set_app(app)


class QCustomPalette(QPalette):
    def __init__(self, primary=PRIMARY, secondary=SECONDARY, disabled=DARK_GRAY,
                 text=WHITE, bright_text=RED, highlighted_text=BLACK, highlight=TERTIARY, tool_tip_base=WHITE, *__args):
        super().__init__(*__args)
        q_primary = QColor(primary)
        q_secondary = QColor(secondary)
        q_disabled = QColor(disabled)
        q_text = QColor(text)
        q_bright_text = QColor(bright_text)
        q_highlighted_text = QColor(highlighted_text)
        q_highlight = QColor(highlight)
        q_tool_tip_base = QColor(tool_tip_base)
        self.setColor(QPalette.Window, q_primary)
        self.setColor(QPalette.WindowText, q_text)
        self.setColor(QPalette.Base, q_secondary)
        self.setColor(QPalette.AlternateBase, q_primary)
        self.setColor(QPalette.ToolTipBase, q_tool_tip_base)
        self.setColor(QPalette.ToolTipText, q_text)
        self.setColor(QPalette.Text, q_text)
        self.setColor(QPalette.Button, q_primary)
        self.setColor(QPalette.ButtonText, q_text)
        self.setColor(QPalette.BrightText, q_bright_text)
        self.setColor(QPalette.Link, q_highlight)
        self.setColor(QPalette.Highlight, q_highlight)
        self.setColor(QPalette.HighlightedText, q_highlighted_text)
        self.setColor(QPalette.Disabled, QPalette.Text, q_disabled)
        self.setColor(QPalette.Disabled, QPalette.ButtonText, q_disabled)

    @staticmethod
    def set_stylesheet(app):
        app.setStyleSheet("QToolTip {{"
                          "color: {white};"
                          "background-color: {tertiary};"
                          "border: 1px solid {white};"
                          "}}".format(white=css_rgb(QColor(WHITE)), tertiary=css_rgb(QColor(TERTIARY))))

    def set_app(self, app):
        app.setStyle("Fusion")
        app.setPalette(self)
        self.set_stylesheet(app)
