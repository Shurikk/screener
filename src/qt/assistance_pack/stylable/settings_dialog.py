from src.qt.assistance_pack.base_dialog_activity import BaseDialogActivity
from src.qt.assistance_pack.stylable.settings_ui import Ui_Dialog
from src.qt.assistance_pack.stylable.styles_handler import get_styles


class SettingsDialog(BaseDialogActivity):
    def __init__(self, current_style, parent=None):
        super(SettingsDialog, self).__init__(Ui_Dialog, parent=parent)
        self.current_style = current_style
        self.ui_setup()
        self.ui_actions_setup()

    def ui_setup(self):
        st = get_styles().copy()
        st.remove(self.current_style)
        styles = [self.current_style, *st]
        self.ui.style_combo_box.addItems(styles)

    def ui_actions_setup(self):
        self.ui.buttonBox.accepted.connect(self.accept)
        self.ui.buttonBox.rejected.connect(self.reject)
        self.ui.style_combo_box.activated.connect(self._style_combo_handle)

    def _style_combo_handle(self, index):
        self.current_style = self.ui.style_combo_box.itemText(index)

    def get_results(self):
        return self.current_style

    def accept(self):
        super().accept()

    def reject(self):
        super().reject()
