from collections import namedtuple

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QFontDialog, QWidget, QDesktopWidget

from src.qt.assistance_pack.base_activity import BaseActivity
from src.qt.assistance_pack.stylable.settings_dialog import SettingsDialog


class StylableActivity(BaseActivity):
    SIZE_BOOST = 1.05

    APP_SHORTCUTS_BASE = namedtuple('AppShortcuts',
                                    ['escape',
                                     'full_screen',
                                     'increase_app',
                                     'decrease_app'])

    def __init__(self, ui_class, parent=None,
                 escape=Qt.Key_Escape, full_screen_key='F10', increase_app='+', decrease_app='-'):
        super().__init__(ui_class=ui_class, parent=parent)
        self.shortcuts = StylableActivity.APP_SHORTCUTS_BASE(escape=escape,
                                                             full_screen=full_screen_key,
                                                             increase_app=increase_app,
                                                             decrease_app=decrease_app)
        self.menubar_setup()

    def menubar_setup(self):
        self.edit_menu_setup()
        self.view_menu_setup()

    def edit_menu_setup(self):
        self.ui.menuEdit.addAction("Set Interface Font...",
                                   lambda: self.update_font())
        self.ui.menuEdit.addAction("Change style",
                                   lambda: self.change_app_style(SettingsDialog(self.current_style).show_dialog()[1]))

    def view_menu_setup(self):
        size_boost = StylableActivity.SIZE_BOOST
        screen_width = QDesktopWidget().availableGeometry().width()
        screen_height = QDesktopWidget().availableGeometry().height()
        self.ui.menuView.addAction("Minimize", lambda: self.showMinimized())
        self.ui.menuView.addAction("Maximize", lambda: self.showMaximized())
        self.ui.menuView.addAction("Default", lambda: self.showNormal())
        self.ui.menuView.addAction("Full-Screen",
                                   lambda: self.showNormal() if self.isFullScreen() else self.showFullScreen(),
                                   shortcut=self.shortcuts.full_screen)
        self.ui.menuView.addSeparator()
        self.ui.menuView.addAction("Center", lambda: self.move_to_center())
        self.ui.menuView.addAction("Top-Left", lambda: self.move(0, 0))
        self.ui.menuView.addAction("Top-Right", lambda: self.move(screen_width - self.size().width(), 0))
        self.ui.menuView.addAction("Bottom-Left", lambda: self.move(0, screen_height - self.size().height()))
        self.ui.menuView.addAction("Bottom-Right", lambda: self.move(screen_width - self.size().width(),
                                                                     screen_height - self.size().height()))
        self.ui.menuView.addSeparator()
        self.ui.menuView.addAction("Increase size",
                                   lambda: self.resize(self.size().width() * size_boost,
                                                       self.size().height() * size_boost),
                                   shortcut=self.shortcuts.increase_app)
        self.ui.menuView.addAction("Decrease size",
                                   lambda: self.resize(self.size().width() // size_boost,
                                                       self.size().height() // size_boost),
                                   shortcut=self.shortcuts.decrease_app)
        self.ui.menuView.addAction("Minimum size", lambda: self.resize(self.minimumSize()))
        self.ui.menuView.addAction("Maximum size", lambda: self.resize(self.maximumSize()))
        self.ui.menuView.addSeparator()
        self.ui.menuView.addAction("Enable/Disable Resize", self.swap_resizable)

    def update_font(self):
        new_font = QFontDialog.getFont(self)[0]
        for l in self.findChildren(QWidget):
            l.setFont(new_font)
