from PyQt5.QtCore import QTime, QTimer
from PyQt5.QtWidgets import QApplication, QLCDNumber


class DigitalClock(QLCDNumber):
    def __init__(self, parent=None):
        super(DigitalClock, self).__init__(parent)
        self.setSegmentStyle(QLCDNumber.Filled)
        self.resize(150, 60)
        self.setDigitCount(10)
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.show_time)
        self.time_data = 0
        self.display('00:00:00')

    def activate(self):
        self.timer.start(1000)
        # self.show_time()

    def deactivate(self):
        self.timer.stop()
        # self.time_data.stop()

    def show_time(self):
        self.time_data = self.time_data + 1
        text = "%02d:%02d:%02d" % (self.time_data / 3600, self.time_data / 60, self.time_data % 60)
        self.display(text)

    def reset(self):
        self.deactivate()
        self.time_data = 0
        self.display('00:00:00')


if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)
    clock = DigitalClock()
    clock.show()
    clock.activate()
    sys.exit(app.exec_())
