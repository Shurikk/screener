import qdarkstyle
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QMainWindow, QDesktopWidget, QAction, QWIDGETSIZE_MAX, QApplication, QMessageBox

from src.qt.assistance_pack.stylable.styles_handler import *


class BaseWindow(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

    def move_to_center(self):
        qt_rectangle = self.frameGeometry()
        center_point = QDesktopWidget().availableGeometry().center()
        qt_rectangle.moveCenter(center_point)
        self.move(qt_rectangle.topLeft())

    def set_resizable(self):
        self.setMaximumSize(QWIDGETSIZE_MAX, QWIDGETSIZE_MAX)
        self.setMinimumSize(0, 0)

    def set_fixed_size(self):
        self.setFixedSize(self.size())

    def swap_resizable(self):
        self.set_fixed_size() if self.minimumSize() != self.maximumSize() else self.set_resizable()


class BaseActivity(BaseWindow):
    def __init__(self, ui_class, parent=None):
        super().__init__(parent=parent)
        self.ui = ui_class()
        self.current_style = None
        self.default_palette = None
        self.setup()

    @staticmethod
    def get_app():
        return QApplication.instance()

    def setup(self):
        self.ui.setupUi(self)
        self.setWindowIcon(QIcon(RESOURCES_DIR + 'main_picture.png'))
        self.move_to_center()
        self.set_resizable()
        self.default_palette = self.get_app().palette()
        self.change_app_style(list(STYLES.keys())[2])

    def create_menu_action(self, action_name, status_tip, trigger_action, icon_path='', shortcut=''):
        action = QAction(QIcon(icon_path), '&' + action_name, self)
        action.setShortcut(shortcut)
        action.setStatusTip(status_tip)
        action.triggered.connect(trigger_action)
        return action

    def change_app_style(self, style):
        self.reset_style()
        app = self.get_app()
        if style == 'Fusion' or style == 'Windows':
            app.setStyle(style)
        elif style == 'Dark-blue':  # TODO
            self.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())
        else:
            apply_style(app, style)
        self.current_style = style
        print(f'Style: {self.current_style}')

    def reset_style(self):
        app = self.get_app()
        app.setStyle("Fusion")
        app.setPalette(self.default_palette)

    def ask_question(self, title='Question', text='Message',
                     buttons=QMessageBox.Yes | QMessageBox.No,
                     default_button=QMessageBox.Yes):
        result = QMessageBox.question(self, title, text, buttons, default_button)
        return result == QMessageBox.Yes

    def show_info(self, title='Info', text='Message'):
        QMessageBox.information(self, title, text)
