from PyQt5.QtWidgets import QDesktopWidget, QDialog


class BaseDialogActivity(QDialog):
    def __init__(self, ui_class, parent=None):
        super().__init__(parent=parent)
        self.ui = ui_class()
        self.setup()

    def setup(self):
        self.ui.setupUi(self)
        self.move_to_center()

    def move_to_center(self):
        qt_rectangle = self.frameGeometry()
        center_point = QDesktopWidget().availableGeometry().center()
        qt_rectangle.moveCenter(center_point)
        self.move(qt_rectangle.topLeft())

    def get_results(self):
        return

    def show_dialog(self):
        result = self.exec_()
        return result == QDialog.Accepted, self.get_results()
