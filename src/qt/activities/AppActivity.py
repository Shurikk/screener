from PyQt5.QtWidgets import QMessageBox

from src.definitions import WINDOW_RESOLUTION
from src.logic.help_logic import IntData
from src.qt.activities.capture_activity import CaptureWindow
from src.qt.assistance_pack.DigitalClockFile import DigitalClock

from src.qt.assistance_pack.stylable.stylable_activity import StylableActivity
from src.qt.qt_builders.start_window_ui import Ui_MainWindow


class AppActivity(StylableActivity):
    STATUS_INIT = 0
    STATUS_CAPTURE = 1
    STATUS_PAUSED = 2

    def __init__(self, parent=None):
        super(AppActivity, self).__init__(Ui_MainWindow, parent=parent)
        self.capture_window = CaptureWindow()
        self.clock = DigitalClock(self)
        self.ui_setup()
        self.ui_actions_setup()
        self.status = IntData(AppActivity.STATUS_INIT)
        self.status.subscribe(self.status_handler)
        self.capture_window.show()

    def closeEvent(self, event):
        is_ok = self.ask_question(title="Confirm dialog",
                                  text="Exiting. Are you sure?",
                                  default_button=QMessageBox.No)
        if is_ok:
            event.accept()
            self.capture_window.close()
        else:
            event.ignore()

    def ui_setup(self):
        self.resize(WINDOW_RESOLUTION['main']['x'], WINDOW_RESOLUTION['main']['y'])
        self.ui.pause_button.setEnabled(False)
        self.ui.stop_button.setEnabled(False)
        self.ui.horizontalLayout.addWidget(self.clock)

    def ui_actions_setup(self):
        self.ui.start_button.clicked.connect(self.launch_capture)
        self.ui.pause_button.clicked.connect(self.pause_resume)
        self.ui.stop_button.clicked.connect(self.stop_capture)

    def launch_capture(self):
        self.status.value = AppActivity.STATUS_CAPTURE
        self.capture_window.set_fixed_size()
        self.capture_window.launch_capture()
        self.ui.pause_button.setEnabled(True)
        self.ui.stop_button.setEnabled(True)
        self.clock.activate()

    def pause_resume(self):
        self.status.value = [AppActivity.STATUS_INIT,
                             AppActivity.STATUS_PAUSED,
                             AppActivity.STATUS_CAPTURE][self.status.value]
        is_paused = self.status.value == AppActivity.STATUS_PAUSED
        self.capture_window.pause(is_paused=is_paused)
        self.clock.deactivate() if is_paused else self.clock.activate()

    def stop_capture(self):
        self.capture_window.stop_capture()
        self.capture_window.set_resizable()
        self.reset()

    def status_handler(self, new_status):
        txt = ['Pause', 'Pause', 'Resume'][self.status.value]
        self.ui.pause_button.setText(txt)
        print(txt)

    def reset(self):
        self.status.value = AppActivity.STATUS_INIT
        self.capture_window.reset()
        self.ui.pause_button.setEnabled(False)
        self.ui.stop_button.setEnabled(False)
        self.clock.reset()
