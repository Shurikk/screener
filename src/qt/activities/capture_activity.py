from collections import namedtuple
from cv2 import VideoWriter
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QMessageBox
from PyQt5 import QtGui
from PyQt5.QtCore import Qt, QPoint, QThread

from src.logic.help_logic import BoolData
from src.logic.screen_capture import capture
from src.qt.assistance_pack.base_activity import BaseWindow

WINDOW_DATA = namedtuple('WindowData', ['width', 'height', 'left_offset', 'top_offset', 'show_fps'])


class CaptureWindow(BaseWindow):
    WINDOW_BORDER = 20

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.setup_ui()
        self.is_capturing = BoolData(False)
        self.is_paused = BoolData(False)
        self.press = False
        self.last_pos = QPoint(0, 0)
        self.capture_thread = CaptureThread(self.is_capturing,
                                            self.is_paused,
                                            self.get_window_data())
        # self.connect(self.capture_thread, QThread.SIGNAL("complete(QString)"), self.add_post)
        # self.connect(self.get_thread, QThread.SIGNAL("finished()"), self.done)

        self.make_frameless()

    def mouseMoveEvent(self, event):
        if self.press:
            self.move(event.globalPos() - self.last_pos)

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.press = True

        self.last_pos = event.pos()

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.press = False

    def paintEvent(self, event: QtGui.QPaintEvent):
        painter = QtGui.QPainter(self)
        painter.setPen(QtGui.QPen(Qt.green, CaptureWindow.WINDOW_BORDER))
        painter.drawRect(self.rect())

    def resizeEvent(self, event):
        super(CaptureWindow, self).resizeEvent(event)

    def setup_ui(self):
        self.setWindowTitle('Capture')
        self.setGeometry(300, 300, 300, 220)
        self.statusBar().showMessage('')

    def make_frameless(self):
        self.setAttribute(Qt.WA_TranslucentBackground, True)
        self.setWindowFlags(Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint)
        # self.setWindowOpacity(0.1)

    def launch_capture(self):
        if bool(self.is_capturing):
            return
        self.is_capturing.value = True
        self.capturing()

    def capturing(self):
        self.capture_thread.window_data = self.get_window_data()
        self.capture_thread.start()

    def stop_capture(self):
        self.is_capturing.value = False

    def pause(self, is_paused):
        self.is_paused.value = is_paused
        self.capturing()

    def get_window_data(self):
        return WINDOW_DATA(width=self.width() - 2 * CaptureWindow.WINDOW_BORDER,
                           height=self.height() - 2 * CaptureWindow.WINDOW_BORDER,
                           left_offset=self.x() + CaptureWindow.WINDOW_BORDER,
                           top_offset=self.y() + CaptureWindow.WINDOW_BORDER,
                           show_fps=False)

    def reset(self):
        self.is_capturing.value = False
        self.is_paused.value = False
        self.press = False
        self.last_pos = QPoint(0, 0)
        self.capture_thread.capture_out = None


class CaptureThread(QThread):

    def __init__(self, is_capturing, is_paused, window_data):
        QThread.__init__(self)
        self.is_capturing = is_capturing
        self.is_paused = is_paused
        self.window_data = window_data
        self.capture_out = None

    def __del__(self):
        self.wait()

    def run(self):
        self.capture_out = capture(frame_width=self.window_data.width,
                                   frame_height=self.window_data.height,
                                   left_offset=self.window_data.left_offset,
                                   top_offset=self.window_data.top_offset,
                                   show_fps=self.window_data.show_fps,
                                   locker=self.is_capturing,
                                   pause=self.is_paused,
                                   cap_out=self.capture_out)

    def done(self):
        QMessageBox.information(self, "Done!", "Done fetching posts!")
        # self.emit(QThread.SIGNAL('complete(VideoWriter)'), self.capture_out)
