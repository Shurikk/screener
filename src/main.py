from PyQt5.QtWidgets import QApplication
import sys

from src.logic.screen_capture import capture
from src.logic.video_converter import convert_to_gif
from src.qt.activities.AppActivity import AppActivity
from src.qt.activities.capture_activity import CaptureWindow

if __name__ == '__main__':
    # capture(frame_width=1920, frame_height=1080, show_fps=True)
    # convert_to_gif('screen_capture.avi')
    app = QApplication(sys.argv)
    main_activity = AppActivity()
    main_activity.show()
    sys.exit(app.exec_())
